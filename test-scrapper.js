const fetchData = require("./src/fetchData");

const siteUrl = `http://www.foodiez.com.bd/Dhaka/Restaurants/Details/?name=Khazana-Restaurant&number=L4DIDE874L`;

const getResults = async () => {
  const data = {};
  try {
    console.log("scrape started...");
    const $ = await fetchData(siteUrl);
    data.name = $(".cover-title-02 > a").text();
    if (!data.name) {
      data.name = $(".title-02 > .listing-rest-name").text();
      data.address = $(".restaurants-contacts > li.description")
        .text()
        .replace(/\s\s+/g, "");
      data.lat = $(".restaurants-contacts > li.description > a")
        .attr("onclick")
        .replace("ShowGoogleMap(", "")
        .split(",")[0];
      data.long = $(".restaurants-contacts > li.description > a")
        .attr("onclick")
        .replace("ShowGoogleMap(", "")
        .split(",")[1];
      data.phone = $(".restaurants-contacts > li.description")
        .next()
        .text()
        .replace(/\s\s+/g, "");
      data.facebook = $(".restaurants-contacts > li > .link-hover").attr(
        "href"
      );
      // console.log(list);
    } else {
      data.address = $("ul.restaurants-address.common-list > li")
        .first()
        .text()
        .replace(/\s\s+/g, "");
      data.phone = $(
        "ul.restaurants-address.common-list > li > .glyphicon-phone-alt"
      )
        .next()
        .html();
      data.facebook = $(
        "ul.restaurants-address.common-list > li > a > .fa-facebook"
      )
        .parent()
        .attr("href");

      data.lat = $(
        "ul.restaurants-address.common-list > li > a > .fa-map-marker"
      )
        .parent()
        .attr("onclick")
        .replace("ShowGoogleMap(", "")
        .split(",")[0];
      data.long = $(
        "ul.restaurants-address.common-list > li > a > .fa-map-marker"
      )
        .parent()
        .attr("onclick")
        .replace("ShowGoogleMap(", "")
        .split(",")[1];
    }
  } catch (error) {
    console.log(error.message);
  }

  console.log("scrape finished", data);
  return data;
};
module.exports = getResults;

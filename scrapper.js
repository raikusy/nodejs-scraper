const fs = require("fs");
const fetchData = require("./src/fetchData");
const getData = require("./src/getData");

const siteUrl = `http://www.foodiez.com.bd/Dhaka/Restaurants/Search?page`;

const getResults = async () => {
  const restaurants = [];
  for (let i = 1; i <= 1; i++) {
    try {
      console.log("scraping page - " + i);
      const $ = await fetchData(`${siteUrl}=${i}`);

      const promises = [];

      $(".white-bg > .result-box").each(function(indx, el) {
        const name = $(this)
          .find(".title-02 > .listing-rest-name")
          .text();
        const area = $(this)
          .find(".title-02 > a > .result-sub-title")
          .text();
        const link = $(this)
          .find(".title-02 > .listing-rest-name")
          .attr("href");

        const promise = getData(link);
        promises.push(promise);
      });
      const data = await Promise.all(promises);
      console.log(data);
      restaurants.push(...data);
    } catch (error) {
      console.log("ERROR HAPPENED BRO!!", error.message);
    }
  }
  let jsonString = JSON.stringify(restaurants, null, 2);
  fs.writeFileSync("../output.json", jsonString, "utf-8");
  console.log("ALL SCRAPPED!!", restaurants);
  return {
    restaurants: restaurants
  };
};
module.exports = getResults;

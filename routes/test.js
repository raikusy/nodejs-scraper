var express = require("express");
var router = express.Router();
const getResults = require("../test-scrapper");

/* GET home page. */
router.get("/", async function(req, res, next) {
  const result = await getResults();
  res.send(result);
});

module.exports = router;

const fetchData = require("./fetchData");

const getData = async siteUrl => {
  const data = {};
  try {
    const $ = await fetchData(siteUrl);
    data.name = $(".cover-title-02 > a").text();
    if (!data.name) {
      data.name = $(".title-02 > .listing-rest-name").text();
      const addr = $(".restaurants-contacts > li.description").text();
      if (addr) {
        data.address = addr.replace(/\s\s+/g, "");
      }
      const lat = $(".restaurants-contacts > li.description > a");

      if (lat) {
        data.lat = lat
          .attr("onclick")
          .replace("ShowGoogleMap(", "")
          .split(",")[0];
      }
      const long = $(".restaurants-contacts > li.description > a");

      if (long) {
        data.long = long
          .attr("onclick")
          .replace("ShowGoogleMap(", "")
          .split(",")[1];
      }
      const phone = $(".restaurants-contacts > li.description");
      if (phone) {
        data.phone = phone
          .next()
          .text()
          .replace(/\s\s+/g, "");
      }
      data.facebook = $(".restaurants-contacts > li > .link-hover").attr(
        "href"
      );
      // console.log(list);
    } else {
      const addr = $("ul.restaurants-address.common-list > li");
      if (addr) {
        data.address = addr
          .first()
          .text()
          .replace(/\s\s+/g, "");
      }
      data.phone = $(
        "ul.restaurants-address.common-list > li > .glyphicon-phone-alt"
      )
        .next()
        .html();
      data.facebook = $(
        "ul.restaurants-address.common-list > li > a > .fa-facebook"
      )
        .parent()
        .attr("href");

      const lat = $(
        "ul.restaurants-address.common-list > li > a > .fa-map-marker"
      )
        .parent()
        .attr("onclick");
      if (lat) {
        data.lat = lat.replace("ShowGoogleMap(", "").split(",")[0];
      }
      const long = $(
        "ul.restaurants-address.common-list > li > a > .fa-map-marker"
      )
        .parent()
        .attr("onclick");
      if (long) {
        data.long = long.replace("ShowGoogleMap(", "").split(",")[1];
      }
    }
  } catch (error) {
    console.log("ERROR WHILE GETTING DATA -> ", error.message);
  }

  console.log("scrape finished - ", data.name);
  return data;
};
module.exports = getData;

const cheerio = require("cheerio");
const axios = require("axios");

const fetchData = async url => {
  try {
    const result = await axios.get(url);
    return cheerio.load(result.data);
  } catch (error) {
    console.log("Cannot load URL!", error.message);
  }
};

module.exports = fetchData;
